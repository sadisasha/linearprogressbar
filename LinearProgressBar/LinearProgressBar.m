//
//  LinearProgressBar.m
//  LinearProgressBar
//
//  Created by Aleksandr Sadikov on 21.01.15.
//  Copyright © 2015 Aleksandr Sadikov. All rights reserved.
//

#import "LinearProgressBar.h"

@implementation LinearProgressBar

@synthesize barColor;
@synthesize barWidth;

@synthesize trackColor;
@synthesize trackWidth;
@synthesize trackPadding;

@synthesize maxValue;
@synthesize value;

#pragma mark - Initializations

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        [self initView];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self initView];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    
    if (self) {
        
        [self initView];
    }
    
    return self;
}

- (void)initView {
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.barColor = [UIColor greenColor];
    self.barWidth = 8.0;
    
    self.trackColor = [UIColor grayColor];
    self.trackWidth = 10.0;
    self.trackPadding = 5.0;
    
    self.maxValue = 100.0;
    self.value = 50.0;
}

#pragma mark - Getters & Setters

- (void)setTrackPadding:(CGFloat)padding {
    
    if (padding < 0) {
        
        trackPadding = 0;
        
    } else if (padding > barWidth) {
        
        trackPadding = 0;
        
    } else {
        
        trackPadding = padding;
    }

    [self setNeedsDisplay];
}

-  (void)setValue:(CGFloat)_value {
    
    if (_value >= maxValue) {
        
        value = maxValue;
        
    } else if (_value <= 0) {
        
        value = 0;
        
    } else {
        
        value = _value;
    }
    
    [self setNeedsDisplay];
}

#pragma mark - drawRect

- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    
    [self drawTrack:rect context:context];
    [self drawBar:rect context:context];
    
    CGContextRestoreGState(context);
    
    if (self.delegate) {
        
        [self.delegate didChangeProgressValue:self.value];
    }
}

#pragma mark - drawRect helper methods

- (void)drawTrack:(CGRect)rect context:(CGContextRef)cxt {
    
    CGContextSetStrokeColorWithColor(cxt, trackColor.CGColor);
    CGContextBeginPath(cxt);
    CGContextSetLineWidth(cxt, barWidth + trackPadding);
    CGContextMoveToPoint(cxt, barWidth, rect.size.height / 2);
    CGContextAddLineToPoint(cxt, rect.size.width - barWidth, rect.size.height / 2);
    CGContextSetLineCap(cxt, kCGLineCapRound);
    CGContextStrokePath(cxt);
}

- (void)drawBar:(CGRect)rect context:(CGContextRef)cxt {

    CGContextSetStrokeColorWithColor(cxt, barColor.CGColor);
    CGContextSetLineWidth(cxt, barWidth);
    CGContextBeginPath(cxt);
    CGContextMoveToPoint(cxt, barWidth, rect.size.height / 2);
    CGContextAddLineToPoint(cxt, barWidth + [self calcualtePercentage:rect], rect.size.height / 2);
    CGContextSetLineCap(cxt, kCGLineCapRound);
    CGContextStrokePath(cxt);
}

- (CGFloat)calcualtePercentage:(CGRect)rect {
    
    CGFloat screenWidth = rect.size.width - (barWidth * 2);
    return (value / 100) * screenWidth;
}

@end

//
//  LinearProgressBar.h
//  LinearProgressBar
//
//  Created by Aleksandr Sadikov on 21.01.15.
//  Copyright © 2015 Aleksandr Sadikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LinearProgressBarDelegate <NSObject>

@optional
- (void)didChangeProgressValue:(double)value;

@end

IB_DESIGNABLE
@interface LinearProgressBar : UIView

@property (nonatomic, copy) IBInspectable UIColor *barColor;
@property (nonatomic, copy) IBInspectable UIColor *trackColor;

@property (nonatomic) IBInspectable CGFloat barWidth;

@property (nonatomic) IBInspectable CGFloat trackWidth;
@property (nonatomic) IBInspectable CGFloat trackPadding;

@property (nonatomic) IBInspectable CGFloat maxValue;
@property (nonatomic) IBInspectable CGFloat value;

@property (nonatomic) IBInspectable id<LinearProgressBarDelegate> delegate;

@end

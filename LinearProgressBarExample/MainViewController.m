//
//  ViewController.m
//  LinearProgressBarExample
//
//  Created by Aleksandr Sadikov on 02.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "MainViewController.h"

#import "LinearProgressBar.h"

@interface MainViewController () <LinearProgressBarDelegate> {
    
    LinearProgressBar *progressBar;
}

@property (strong, nonatomic) IBOutlet LinearProgressBar *linearPrgressBar;
@property (strong, nonatomic) IBOutlet UISlider *slider;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.linearPrgressBar.delegate = self;
    
    CGRect frame = self.linearPrgressBar.frame;
    frame.size.width = self.view.bounds.size.width - 40.0;
    frame.origin.y += 40.0;
    
    progressBar = [[LinearProgressBar alloc] initWithFrame:frame];

    progressBar.trackColor = self.linearPrgressBar.trackColor;
    progressBar.trackWidth = self.linearPrgressBar.trackWidth;
    progressBar.trackPadding = self.linearPrgressBar.trackPadding;
    
    progressBar.barColor = [UIColor redColor];
    progressBar.barWidth = self.linearPrgressBar.barWidth;
    
    progressBar.maxValue = self.linearPrgressBar.maxValue;
    progressBar.value = self.linearPrgressBar.value;
    
    [self.view addSubview:progressBar];
    
    [self.slider setValue:progressBar.value / 100];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSliderValueChanged:(UISlider *)sender {

    [self.linearPrgressBar setValue:sender.value * 100];
}

- (void)didChangeProgressValue:(double)value {
    
    [progressBar setValue:value];
}

@end
